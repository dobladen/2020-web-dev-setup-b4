# 2020-Web-Dev-Setup-B4

This is a repo for our developers based on Gulp, TypeScript/JS/ES6+ and Sass. With Bootstrap 4. Furthermore we have included FontAwesome and we are loading Google Fonts locally.

## Console Commands

Clone repo:
```
git clone https://gitlab.com/dobladen/2020-web-dev-setup-b4.git
```

**IMPORTANT:** Check that you have `npm` (will be installed with Node.js) installed. You can donwload it here: https://nodejs.org/en/download/ 

Enter this command to install the node-modules
```
npm install
```

The following command starts the environment and builds everything once and waits for changes. A liveserver including browser refresh is started.
```
npm run dev
```

The following command is the build command where everything is built and stored in the `dist/` directory. It does not wait for changes to files.
```
npm run build
```

### Thanks to
Based on the repo from Unleashed Design: https://github.com/Unleashed-Design/basic-dev-environment-2020